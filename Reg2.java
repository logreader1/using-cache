package regex;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Regex{
    static Scanner scanner = new Scanner(System.in);
    static File fil = new File("D:\\logs");
    static File f[] = fil.listFiles();
    static ArrayList<Future<?>> futures = new ArrayList<Future<?>>();
    static ArrayList<String> date = new ArrayList<String>();
    static ArrayList<String> exception = new ArrayList<String>();
    static HashMap<String, HashMap<String,Integer>> hmap = new HashMap<String, HashMap<String,Integer>>();


    static ExecutorService th = Executors.newFixedThreadPool(5);


    static void keywordtofile(){
        System.out.println("Enter the keyword to be fetched");
        String date = scanner.nextLine();
        for (File file : f){
            Runnable reg1 = new Runnable(){
                public void run(){
                    try{
                        Scanner in = new Scanner(file);
                        System.out.println("\n\n\nThis date is present on " + file.getName() + "\n\n\n");
                        while (in.hasNextLine()) {
                            String line = in.nextLine();
                            if (Pattern.compile(date).matcher(line).find()) {
                                System.out.println(line + " " + Thread.currentThread().getName());
                            }
                        }
                    } catch (FileNotFoundException e){
                        e.printStackTrace();
                    }
                }
            };
            Future<?> fur = th.submit(reg1);
            futures.add(fur);
        }
    }

    static void datetofilecontent() throws IOException {
        @SuppressWarnings("resource")
        FileWriter filewriter = new FileWriter("D:\\temp\\temp.txt");
        System.out.println("Enter the Date to read the file content :-   Enter in the format xx-xx-xxx");
        String dates = scanner.nextLine();
        if (Pattern.matches("^\\d{2}-\\d{2}-\\d{4}$", dates)){
            date.add(dates);
            for (File file : f) {
                Runnable reg3 = new Runnable(){
                    public void run(){
                        Scanner in;
                        try {
                            in = new Scanner(file);
                            while (in.hasNextLine()) {
                                String line = in.nextLine();
                                Pattern pm = Pattern.compile(dates);
                                Matcher mp = pm.matcher(line);
                                if (mp.find()) {
                                    System.out.println(line);
                                    filewriter.write(line+"\n");
                                    while (in.hasNextLine()) {
                                        String l = in.nextLine();
                                        Pattern p = Pattern.compile("\\d{2}-\\d{2}-\\d{4}");
                                        Matcher m = p.matcher(l);
                                        if (m.find())
                                            break;
                                        else {
                                            System.out.println(l);
                                            filewriter.write(l+"\n");
                                        }


                                    }
                                }
                            }
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                };
                Future<?> fur = th.submit(reg3);
                futures.add(fur);
            }
        } else
            System.out.println("The Date is not in Proper Format");
    }

    static void datetoexception() {
        System.out.println("Enter the Date to fetch the exceptions occurs :-   Enter in the format xx-xx-xxx\n");
        String dat = scanner.nextLine();
        if (Pattern.matches("^\\d{2}-\\d{2}-\\d{4}$", dat)) {
            if(date.contains(dat)) {
                File file = new File("D:\\temp\\temp.txt");
                common(dat,file);
            }
            else {
                for (File file : f)
                    common(dat,file);
            }
        } else {
            System.out.println("The Date is not in Proper Format");
        }
    }

    private static void common(String dat,File file) {

        Runnable reg2 = new Runnable() {
            public void run(){
                try {
                    Scanner in = new Scanner(file);
                    while (in.hasNextLine()) {
                        String line = in.nextLine();
                        Pattern pm = Pattern.compile(dat);
                        Matcher mp = pm.matcher(line);
                        if (mp.find()) {
                            while (true) {
                                if (in.hasNextLine()) {
                                    String l = in.nextLine();
                                    Pattern p = Pattern.compile("\\d{2}-\\d{2}-\\d{4}");
                                    Matcher m = p.matcher(l);
                                    if (m.find())
                                        break;
                                    else {
                                        Pattern pat = Pattern.compile(":.*?Exception");
                                        Matcher mat = pat.matcher(l);
                                        if (mat.find())
                                            System.out.println("Exception " + mat.group()
                                                    + " occurs in " + file.getName());
                                    }
                                }
                            }
                        }
                    }

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        };
        Future<?> fur = th.submit(reg2);
        futures.add(fur);
    }

    static void common1(String ex) throws FileNotFoundException {	
        for(File file : f) {
            if(hmap.get(ex).get(file.getName())!=0) {
                System.out.println("File name :- "+file.getName()+" Count :- "+hmap.get(ex).get(file.getName()));
            }
        }
    }

    static void exceptiontofilename() throws FileNotFoundException {

        System.out.println("Enter the exception to track the file name :- Enter in the format of xxxx.xxxx.xxxxException");
        String ex = scanner.nextLine();
        if (Pattern.matches(".*?Exception", ex)) {
            if(exception.contains(ex)) {
                common1(ex);
            }
            else {
                for (File file : f) {
                    int count = 0, present = 0;
                    @SuppressWarnings("resource")
                    Scanner in = new Scanner(file);
                    while (in.hasNextLine()) {
                        String ls = in.nextLine();
                        Pattern pt = Pattern.compile(ex);
                        Matcher mt = pt.matcher(ls);
                        if (mt.find()) {
                            present = 1;
                            count++;
                        }
                    }
                    if (present == 1)
                        System.out.println(ex + " is present in the File " + file.getName() + " " + count);
                }
            }
        } else {
            System.out.println("Enter the valid Exception");
        }
    }

    static void exceptiontostacktrace() throws IOException {
        HashMap<String,Integer> fname = new HashMap<String,Integer>();
        System.out.println("Enter the Exception :- Enter in the format of xxxx.xxxx.xxxxException");
        String ex1 = scanner.nextLine();
        if (Pattern.matches(".*?Exception", ex1)){
            exception.add(ex1);
            for (File file : f){
                Runnable reg5 = new Runnable() {
                    public void run() {
                        Scanner in;
                        try {
                            int count=0;
                            in = new Scanner(file);
                            while (in.hasNextLine()) {
                                String line = in.nextLine();
                                Pattern pm = Pattern.compile(ex1);
                                Matcher mp = pm.matcher(line);
                                if (mp.find()){
                                    count++;
                                    System.out.println(line);
                                    while (in.hasNextLine()) {
                                        String l = in.nextLine();
                                        Pattern p = Pattern.compile("at|Caused");
                                        Matcher m = p.matcher(l);
                                        if (m.find())
                                            System.out.println(l);
                                        else
                                            break;
                                    }
                                }
                            }
                            fname.put(file.getName(),count);
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                };
                Future<?> fur = th.submit(reg5);
                futures.add(fur);
            }
        }
        else {
            System.out.println("Your Exception is not in Proper format");
        }
        hmap.put(ex1,fname);
    }

    static void exceptions() {
		System.out.println("1.java.lang.Exception\n2.java.sql.SQLException\n3.com.zoho.cp.NoPermitsAvailableException\n4.java.io.IOException\n5.java.lang.NullPointerException");	
	}
}

public class Reg2 {
    @SuppressWarnings("resource")
    public static void main(String[] args) throws ExecutionException, InterruptedException, IOException{
        Scanner scanner = new Scanner(System.in);
        new PrintWriter(new FileWriter("D:\\temp\\temp.txt")).flush();
        String n="-1";
        while (!n.equals("6")){
            System.out.println("\n\n1.keyword to full line\n2.date to file content\n3.Date to exception\n4.exception to stackTrace\n5.exception to filename\n6.End");
            System.out.println("\nEnter task to be performed\n");
            n=scanner.nextLine();
            switch(n){
                case "1":
                    Regex.keywordtofile();
                    break;
                case "2":
                    Regex.datetofilecontent();
                    break;
                case "3":
                    Regex.datetoexception();
                    break;
                case "4":
                    Regex.exceptiontostacktrace();
                    break;
                case "5":
                    Regex.exceptiontofilename();
                    break;
                case "6":
                    System.out.println("Your program Has been Ended");
                    new PrintWriter(new FileWriter("D:\\temp\\temp.txt")).flush();
                    System.out.println("-----------Cache Cleared---------");
                    break;
                default:
                    System.out.println("Enter a valid input");
            }
            for (Future<?> fu : Regex.futures)
                fu.get();
        }


    }
}